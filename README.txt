Esther created the bitbucket account and came up with the idea to use random
moves to create exampleplayer. Coded out update_possMoves and pseudocoded helper
function is_Occupied.

Edward set up the othello repositories and coded exampleplayer following the
random algorithm of exampleplayer. Drafted pseudocode for update_possMoves and
coded helper function is_Occupied.

As a team, both members debugged each others' code.

Improvement documentation:
As a very base case to simply get games going, we programmed our player
to apply random, valid moves until the game was complete. In order to
compete on a higher level, however, we attempted to make a decision tree
to rank each possible move, looking several moves ahead to determine 
the best possible move at each step. First, we wrote update_possMoves, which
updates the vector possMoves each time it is called. This allows us to be able
to loop through this vector to find the move that is the best. This function
also will be helpful when implemented in the decision tree so that there
is a limited amount of moves to loop through, reducing the time it takes to 
compute the decision tree. This strategy will work because it will allow
the program to look ahead of its opponent and judge the effectiveness of
every single one of its moves. We attempted to create a decision tree, but
there were too many bugs and we decided to scrap it.